from django.apps import apps
from django.core.management.base import BaseCommand, CommandError
from apps.models import ExchangeRate
from apps.rate_scraper import RateScraper
import pprint

class Command(BaseCommand):
    
    def store_new_exchange_rate(self):
        ExchangeRate.objects.all().delete()
        new_rates = RateScraper().scrap()

        for new_rate in new_rates:
            ExchangeRate(name=new_rate['name'], rate=new_rate['rate'], diff=new_rate['diff']).save()
    
    def handle(self, *args, **kwargs):
        try:
            self.store_new_exchange_rate()
            self.stdout.write(self.style.SUCCESS("Successfully update exchange rate"))
        except Exception as e:
            self.stderr.write(self.style.ERROR("Error while updating exchange rate"))
            raise CommandError(e)
            
