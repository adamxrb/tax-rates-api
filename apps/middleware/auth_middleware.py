import jwt
import json

from django.http import HttpResponse

class AuthMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        self.secret_key = 'getsugatensho'
        self.secret_msg = 'Alien has invaded the earth!'
    
    def __call__(self, request):
        if request.method != 'POST':
            return self.get_response(request)

        auth_header = request.META['HTTP_AUTHORIZATION']
        token = auth_header.replace('88HEAD ', '')

        try:
            decoded = jwt.decode(token, self.secret_key, algorithms=['HS256'])

            if decoded['secmsg'] == self.secret_msg:
                response = self.get_response(request)
            else:
                response = HttpResponse('Unauthorized xD', status=401)

        except Exception as e:
            response = HttpResponse('Unauthorized xD', status=401)

        return response