from django.db import models

# Create your models here.
class ExchangeRate(models.Model):
    name = models.CharField(max_length=255)
    rate = models.DecimalField(max_digits=12, decimal_places=2)
    diff = models.DecimalField(max_digits=12, decimal_places=2)
