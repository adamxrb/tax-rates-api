import requests
import locale
from bs4 import BeautifulSoup

class RateScraper:
    'Scraper class to extract information of Indonesia tax exchange rate'

    def scrap(self):
        URL = 'https://fiskal.kemenkeu.go.id/informasi-publik/kurs-pajak'
        page = requests.get(URL)

        soup = BeautifulSoup(page.content, 'html.parser')

        results = soup.find('tbody').find_all('tr')

        rates = []
        for row in results:
            rate = row.find_all('td')

            rates.append({
                'name': rate[1].get_text().strip(),
                'rate': self.id_string_to_decimal(rate[2].get_text().strip()),
                'diff': self.id_string_to_decimal(rate[3].get_text().strip())
            })
        
        return rates
    
    def id_string_to_decimal(self, num):
        num = num.replace('.', '')
        num = num.replace(',', '.')
        
        return float(num)

