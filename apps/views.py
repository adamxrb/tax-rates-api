from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.core import serializers
from django.forms.models import model_to_dict

import json

from apps.models import ExchangeRate
from apps.rate_scraper import RateScraper

class HomeView(TemplateView):
    template_name = 'home.html'

def get_currency_rate(request, country_name):
    if request.method != 'POST':
        return HttpResponse('Method Not Allowed', status=405)

    try:
        currency_rate = ExchangeRate.objects.get(name=country_name)
        response = serializers.serialize('json', [currency_rate])
    except Exception as e:
        response = json.dumps([{'Error': 'Country name not found'}])
        raise Exception(e)

    return HttpResponse(response, content_type="text/json")

def get_country_name(request):
    if request.method != 'POST':
        return HttpResponse('Method Not Allowed', status=405)

    country_name = ExchangeRate.objects.values_list('name', flat=True)
    response = json.dumps(list(country_name))

    return HttpResponse(response, content_type="text/json")