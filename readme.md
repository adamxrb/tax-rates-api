# 88Spares Test Submission

## Installation
Please go to repository directory, then:

```
$ pipenv install
$ python manage.py migrate
$ python manage.py update_exchange_rate
$ python manage.py runserver
```

Open browser and access `localhost:8000`

## Explanation

### Rest API

There is 2 API in this apps:

- `api/countries` to fetch all countries name
- `api/rate/<country-name>` to fetch exchange information for specific country

both API can only be called using `POST` because the way I implemented authentication using
global middleware `auth_middleware.py` which the mechanism is to check for `Authorization`
header for all `POST` request and then the static token, which is:

```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZWNtc2ciOiJBbGllbiBoYXMgaW52YWRlZCB0aGUgZWFydGghIn0.MMNRXWMfuL11xpHtLd8MnOc_7MKBOaR71-HkX3uDp0Q
```

Will be verified with `HMAC SHA-256` algorithm. The usual `Bearer` string in `Authorization`
header replaced by `88HEAD`.

### Custom Django Command `update_exchange_rate`

I've created custom class `RateScraper` to actually scrap the data from requested web page
and to update exchange rate database simply invoke `python manage.py update_exchange_rate`
from your terminal.

### Home Page

The home page to find desired country exchange rate built using Vue.js, Bulma CSS, and Axios
for calling REST API.
